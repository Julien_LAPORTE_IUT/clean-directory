import os
import datetime
import sys
import logging
from pathlib import Path

logging.basicConfig(filename='clean_directory.log', level=logging.INFO)

# Calculate the cutoff date for deletion
def calculate_cutoff_date(days_before_deletion):
    current_date = datetime.datetime.now()
    return current_date - datetime.timedelta(days=days_before_deletion)

# Check if the file should be deleted
def should_delete_file(file_path, cutoff_date):
    file_creation_date = datetime.datetime.fromtimestamp(os.path.getctime(file_path))
    return file_creation_date < cutoff_date

# Delete the file
def delete_file(file_path):
    try:
        os.remove(file_path)
        logging.info(f"File deleted: {file_path}")
    except Exception as e:
        logging.error(f"Error deleting file {file_path}: {e}")

# Clean the directory
def clean_directory(folder, days_before_deletion):
    try:
        cutoff_date = calculate_cutoff_date(days_before_deletion)
        folder_path = Path(folder)

        for file in folder_path.iterdir():
            if file.is_file() and should_delete_file(file, cutoff_date):
                delete_file(file)

        logging.info(f"Directory cleaned: {folder_path}")

    except Exception as e:
        logging.error(f"An error occurred: {e}")

# Check if the script is run with sudo privileges
if os.geteuid() != 0:
    print("Please run this script with sudo privileges.")
    sys.exit(1)

try:
    # Get the folder to clean and the number of days before deletion
    folder_to_clean = input("Please enter the path of the folder to clean: ")

    if not Path(folder_to_clean).is_dir():
        raise ValueError("The folder does not exist or is not a directory.")

    # Ask the user for the number of days before deletion
    days_before_deletion = int(input("Please enter the number of days before deletion: "))

    # Check if the number of days is valid
    if days_before_deletion < 0:
        raise ValueError("Number of days must be non-negative.")

    clean_directory(folder_to_clean, days_before_deletion)

except ValueError as value_error:
    print(f"Invalid input: {value_error}")
except KeyboardInterrupt:
    print("Script interrupted by the user.")
    sys.exit(1)
except Exception as e:
    print(f"An error occurred: {e}")
