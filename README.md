# Clean Directory

## Content

- [Application Overview](#application-overview)
- [Installation](#installation)
- [Usage](#usage)

### Application Overview
The "Clean Directory" script is a Python application designed to help you manage and clean up files within a specified directory. It allows you to specify a cutoff date, and any files created before that date will be deleted.

### Installation
1. Clone the repository:
```bash
https://gitlab.com/Julien_LAPORTE_IUT/clean-directory.git
```

2. Enter in the clone directory:
```bash
cd clean-directory
```

### Usage
1. Run the script.
2. Enter the directory to clean
3. Enter the number of day